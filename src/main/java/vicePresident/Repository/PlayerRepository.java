package vicePresident.Repository;

import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import vicePresident.Entity.Club;
import vicePresident.Entity.Player;

import java.util.List;

@Repository
public class PlayerRepository {
    @Autowired
    private SessionFactory sessionFactory;

    @Autowired
    private ClubRepository clubRepo;

    private static Logger LOG = LoggerFactory.getLogger(PlayerRepository.class);

    public Player getPlayerByTGID(Long telegramID) {
        List<Player> playersByTGID = sessionFactory
                .getCurrentSession()
                .createQuery("from Player where telegramID = %d".formatted(telegramID), Player.class)
                .getResultList();
        LOG.debug("found list of players with telegramID = " + telegramID + ": " + playersByTGID);
        return playersByTGID.isEmpty() ? null : playersByTGID.get(0);
    }

    public Player getPlayerByTag(String tag) {
        List<Player> players = sessionFactory
                .getCurrentSession()
                .createQuery("from Player where inGameTag = '%s'".formatted(tag), Player.class)
                .getResultList();

        LOG.debug("found list of players with tag " + tag + ": " + players);
        return players.isEmpty() ? null : players.get(0);
    }

    public void savePlayer(Player player) {
        sessionFactory
                .getCurrentSession()
                .persist(player);
        LOG.debug("player " + player + " saved");
    }

    public void deletePlayer(Player player) {
        LOG.debug(player.getId() + "");
        sessionFactory
                .getCurrentSession()
                .createNativeQuery("delete from player where id = " + player.getId())
                .executeUpdate();
    }
}
