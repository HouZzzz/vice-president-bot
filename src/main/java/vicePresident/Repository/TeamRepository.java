package vicePresident.Repository;

import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import vicePresident.Entity.Team;

import java.util.List;

@Repository
public class TeamRepository {
    @Autowired
    private SessionFactory sessionFactory;

    private static Logger LOG = LoggerFactory.getLogger(TeamRepository.class);


    public List<Team> getTeamsByClubID(int id) {
        List<Team> teams = sessionFactory
                .getCurrentSession()
                .createQuery("from Team where club.id = " + id, Team.class)
                .getResultList();
        LOG.debug("found list of team in club with id " + id + ": " + teams);
        return teams;
    }

    public void saveTeam(Team team) {
        LOG.info("team with club id " + team.getClub().getId() + " modified or created");
        sessionFactory
                .getCurrentSession()
                .persist(team);
    }

    public List<Team> getTeamsByClubChatID(Long chatId) {
        List<Team> teams = sessionFactory
                .getCurrentSession()
                .createQuery("from Team where club.chatID = " + chatId, Team.class)
                .getResultList();
        LOG.debug("found list of teams by chatID " + chatId + ": " + teams);
        return teams;
    }
}
