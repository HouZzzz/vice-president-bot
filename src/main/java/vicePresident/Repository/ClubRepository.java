package vicePresident.Repository;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import vicePresident.Entity.Club;
import vicePresident.Entity.ClubSettings;
import vicePresident.Entity.Player;
import vicePresident.Wrapper.MessageWrapper;

import java.io.Serializable;
import java.util.List;

@Repository
public class ClubRepository {
    private Logger LOG = LoggerFactory.getLogger(ClubRepository.class);

    @Autowired
    private SessionFactory sessionFactory;


    @Transactional
    public List<Club> getAllClubsTransactional() {
        return sessionFactory
                .getCurrentSession()
                .createQuery("from Club", Club.class)
                .getResultList();
    }
    public void saveClub(Club club) {
        Session session = sessionFactory.getCurrentSession();
        session.saveOrUpdate(club);

        LOG.info("saved club: " + club);
    }

    public Club getClubByTGID(Long telegramID) {
        List<Club> club = sessionFactory
                .getCurrentSession()
                .createQuery("from Club where chatID = %d".formatted(telegramID), Club.class)
                .getResultList();

        LOG.debug("found clubs with telegramID " + telegramID + ": " + club);
        return club.isEmpty() ? null : club.get(0);
    }

    public Club getClubByTag(String tag) {
        List<Club> clubs = sessionFactory
                .getCurrentSession()
                .createQuery("from Club where inGameTag = '%s'".formatted(tag), Club.class)
                .getResultList();
        LOG.debug("found clubs with tag " + tag + ": " + clubs);
        return clubs.isEmpty() ? null : clubs.get(0);
    }
    public void unlinkClub(Long chatID) {
        sessionFactory.getCurrentSession()
            .delete(getClubByTGID(chatID));
        LOG.info("club with chatID " + chatID + " was deleted with players (unlinked)");
    }

    public void switchNotify(ClubSettings settings) {
        settings.setNotify(!settings.isNotify());
        LOG.debug("switching notify to " + settings.isNotify() + " in club " + settings.getClub().getInGameTag());

        sessionFactory.getCurrentSession()
                .update(settings);
    }
}
