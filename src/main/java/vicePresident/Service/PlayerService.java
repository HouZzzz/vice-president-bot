package vicePresident.Service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.telegram.telegrambots.meta.api.objects.User;
import vicePresident.Communication.ClubCommunication;
import vicePresident.Communication.PlayerCommunication;
import vicePresident.Entity.Club;
import vicePresident.Entity.Player;
import vicePresident.Entity.Team;
import vicePresident.Repository.ClubRepository;
import vicePresident.Repository.PlayerRepository;
import vicePresident.Wrapper.BrawlStarsEntity.BrawlStarsClub;
import vicePresident.Wrapper.BrawlStarsEntity.BrawlStarsPlayer;
import vicePresident.Wrapper.BrawlStarsEntity.BrawlStartClubMember;
import vicePresident.Wrapper.MessageWrapper;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class PlayerService {
    @Autowired
    private PlayerRepository playerRepo;

    @Autowired
    private ClubRepository clubRepo;

    @Autowired
    private ClubCommunication clubCommunication;

    @Autowired
    private TeamService teamService;

    @Autowired
    private PlayerCommunication playerCommunication;

    private static Logger LOG = LoggerFactory.getLogger(PlayerService.class);

    @Transactional
    public List<MessageWrapper> registerPlayer(String playerTag, Long telegramID,Long clubTelegramID,String playerUsername) {
        Player playerByID = playerRepo.getPlayerByTGID(telegramID);
        Player playerByTag = playerRepo.getPlayerByTag(playerTag);
        // player already registered
        if (playerByID != null || playerByTag != null) {
            return List.of(new MessageWrapper("Упс, <a href=\"tg://user?id=%d\">игрок</a> уже зарегистрирован здесь или в другом клубе.".formatted(telegramID)));
        }

        Club clubByTGID = clubRepo.getClubByTGID(clubTelegramID);
        try {
            BrawlStarsClub ingameClub = clubCommunication.getClubByTag(clubByTGID.getInGameTag());
            boolean stillMember = ingameClub
                    .getMembers()
                    .stream()
                    .anyMatch(m -> m.getTag().toUpperCase().equals(playerTag.toUpperCase()));
            if (!stillMember) {
                return List.of(new MessageWrapper("Упс, в клубе не нашлось такого игрока!"));
            }
        } catch (URISyntaxException e) {LOG.error(e.getMessage());}

        Player player = new Player();
        player.setClub(clubByTGID);
        player.setInGameTag(playerTag);
        player.setTelegramID(telegramID);

        LOG.info("registering player to " + clubByTGID.getInGameTag() + " club");
        player = teamService.findTeam(player);
        return List.of(new MessageWrapper("Пользователь @" + playerUsername + " успешно привязался к аккаунту с тегом " + playerTag));
    }

    @Transactional
    public Player getPlayerByTGID(Long telegramID) {
        return playerRepo.getPlayerByTGID(telegramID);
    }

    @Transactional
    public void deletePlayer(Long telegramID) {
        LOG.debug("deleting player with telegramID " + telegramID);
        playerRepo.deletePlayer(playerRepo.getPlayerByTGID(telegramID));
    }

    @Transactional
    public Team changePlayerTeam(Long telegramID) {
        LOG.debug("changing team for user with telegramID " + telegramID);
        Player player = playerRepo.getPlayerByTGID(telegramID);
        // fixme delete old team if there is 0 players
        int oldTeamID = player.getTeam().getId();

        List<Team> clubTeams = teamService.getTeamsByClubChatID(player.getClub().getChatID());
        Team freeTeamWithMostPlayersCount = clubTeams.stream()
                .filter(t -> t.getId() != oldTeamID && t.getPlayers().size() < 3)
                .sorted((t2, t1) -> t1.getPlayers().size() - t2.getPlayers().size())
                .findFirst()
                .orElse(null);
        LOG.debug("found free team with most player count: " + freeTeamWithMostPlayersCount);

        if (freeTeamWithMostPlayersCount == null) { // all existing teams have 3 players
            Team newTeam = new Team();
            newTeam.getPlayers().add(player);
            newTeam.setClub(player.getClub());
            player.setTeam(newTeam);
            playerRepo.savePlayer(player);

            return newTeam;
        } else {
            freeTeamWithMostPlayersCount.getPlayers().add(player);
            player.setTeam(freeTeamWithMostPlayersCount);
            playerRepo.savePlayer(player);

            return freeTeamWithMostPlayersCount;
        }
    }

    @Transactional
    public List<MessageWrapper> getPlayerTeam(User from) {
        StringBuilder text = new StringBuilder("<b>Ваша команда: \n\n");
        Player player = playerRepo.getPlayerByTGID(from.getId());
        try {
            BrawlStarsClub ingameClub = clubCommunication.getClubByTag(player.getClub().getInGameTag());

            List<Player> team = player.getTeam().getPlayers();
            team.forEach(p -> {
                String nick = ingameClub
                        .getMembers()
                        .stream()
                        .filter(m -> m.getTag().equals(p.getInGameTag()))
                        .map(m -> m.getName())
                        .collect(Collectors.joining());
                text.append("<a href=\"tg://user?id=%d\">%s</a>\n".formatted(p.getTelegramID(),nick));
            });
        } catch (URISyntaxException e) {
            LOG.error(e.getMessage());
            return List.of(new MessageWrapper("На сервере Supercell произошла ошибка, попробуйте позже"));
        }
        return List.of(new MessageWrapper(text + "</b>"));
    }

    // returning poll args
    @Transactional
    public List<Object> play(Long telegramID) {
        List<Object> args = new ArrayList<>(4);
        // [0] long chatID
        // [1] String question
        // [2] List<String> options
        // [3] boolean multianswer

        Player player = playerRepo.getPlayerByTGID(telegramID);
        Team team = player.getTeam();
        if (team.isPlayedToday()) return null; // this team is already played today, sending message from handler

        try {
            BrawlStarsClub ingameClub = clubCommunication.getClubByTag(player.getClub().getInGameTag());

            List<String> teamTags = team.getPlayers()
                    .stream()
                    .map(p -> p.getInGameTag())
                    .toList();

            //          list will contain only one club member
            Map<String, List<BrawlStartClubMember>> tagAndMember = ingameClub.getMembers()
                    .stream()
                    .filter(m -> teamTags.contains(m.getTag()))
                    .collect(Collectors.groupingBy(BrawlStartClubMember::getTag));

            List<String> options = teamTags.stream()
                    .map(t -> tagAndMember.get(t).get(0).getName())
                    .collect(Collectors.toList());

            options.add("1 победа");
            options.add("2 победы");
            options.add("3 победы");

            args.add(player.getClub().getChatID());
            args.add("Выберите тех, кто играл с вами и сколько вы сделали побед.");
            args.add(options);
            args.add(true);
        } catch (URISyntaxException e) {
            LOG.error(e.getMessage());
        }

        return args;
    }

    public String getStringifyPlayerByTag(String tag) {
        try {
            BrawlStarsPlayer player = playerCommunication.getPlayerByTag(tag);

            StringBuilder top3brawlers = new StringBuilder();
            player.getBrawlers().stream()
                    .sorted((b2,b1) -> b1.getTrophies() - b2.getTrophies())
                    .limit(5)
                    .map(b -> "%s\n\uD83C\uDFC6%d (%d ранг)\n\n".formatted(b.getName(),b.getTrophies(),b.getRank()))
                    .forEach(s -> top3brawlers.append(s));


            return """
                    <b>%s</b>
                    
                    🏆%d (максимум 🏆%d)
                    
                    Трио победы: %d
                    Дуо победы: %d
                    Соло победы: %d
                    
                    Топ 3 бравлера по кубкам:
                    %s
                    """.formatted(
                            player.getName(),
                            player.getTrophies(),
                            player.getHighestTrophies(),
                            player.get3vs3Victories(),
                            player.getDuoVictories(),
                            player.getSoloVictories(),
                            top3brawlers.toString());

        } catch (URISyntaxException e) {
            LOG.error(e.getMessage());
            return "Ошибка со стороны сервера :(\nЕсли проблема будет повтораяться обратитесь к разработчику бота (@H0uZz)";
        }

    }
}
