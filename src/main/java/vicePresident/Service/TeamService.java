package vicePresident.Service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vicePresident.Communication.ClubCommunication;
import vicePresident.Entity.Player;
import vicePresident.Entity.Team;
import vicePresident.Repository.ClubRepository;
import vicePresident.Repository.PlayerRepository;
import vicePresident.Repository.TeamRepository;
import vicePresident.Wrapper.BrawlStarsEntity.BrawlStartClubMember;
import vicePresident.Wrapper.MessageWrapper;

import java.net.URISyntaxException;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Service
public class TeamService {
    @Autowired
    private TeamRepository teamRepo;

    @Autowired
    private PlayerRepository playerRepo;

    @Autowired
    private ClubCommunication clubCommunication;

    @Autowired
    private ClubService clubService;

    private static Logger LOG = LoggerFactory.getLogger(TeamService.class);

    @Transactional
    public Player findTeam(Player player) {
        List<Team> clubTeams = teamRepo.getTeamsByClubID(player.getClub().getId());

        Team availableTeam = clubTeams
                .stream()
                .filter(t -> t.getPlayers().size() < 3)
                .sorted((t2,t1) -> t1.getPlayers().size() - t2.getPlayers().size())
                .findFirst()
                .orElseGet(() -> {
                    Team team = new Team();
                    team.setEarnedTrophies(-1); // marking: 0 available teams found
                    return team;
                });

        LOG.debug("available team: " + availableTeam);

        if (availableTeam.getEarnedTrophies() == -1) { // create new cause can't find available
            Team newTeam = new Team();
            newTeam.setClub(player.getClub());

            teamRepo.saveTeam(newTeam);

            player.setTeam(newTeam);
            playerRepo.savePlayer(player);

            return player;
        }

        player.setTeam(availableTeam);
        teamRepo.saveTeam(availableTeam);
        playerRepo.savePlayer(player);

        return player;
    }

    @Transactional
    public List<Team> getTeamsByClubChatID(Long chatId) {
        return teamRepo.getTeamsByClubChatID(chatId);
    }


    public List<MessageWrapper> teamListToMessage(List<Team> teams) {
        Long chatID = teams.get(0).getClub().getChatID();

        StringBuilder text = new StringBuilder("<b>Команды вашего клуба:</b>\n");

        try {
            List<BrawlStartClubMember> members = clubCommunication.getClubByTag(
                    teams.size() != 0 ?
                            teams.get(0).getClub().getInGameTag()
                            :
                            clubService.getClubByChatID(chatID).getInGameTag()
            ).getMembers();

            AtomicInteger index = new AtomicInteger(1);
            teams.forEach(team -> {
                String nicksOfTeamPlayers = team.getPlayers()
                        .stream()
                        .map(player -> {
                            BrawlStartClubMember igMember = members
                                    .stream()
                                    .filter(member -> member.getTag().equals(player.getInGameTag()))
                                    .findFirst()
                                    .get();
                            return "<a href=\"tg://user?id=%d\">%s</a>"
                                    .formatted(player.getTelegramID(),igMember.getName());
                        })
                        .collect(Collectors.joining(", "));
                text.append(index.getAndIncrement()).append(") ").append(nicksOfTeamPlayers).append("\n");
            });
        } catch (URISyntaxException e) {
            LOG.error(e.getMessage());
            return List.of(new MessageWrapper("Произошла ошибка со стороны сервера Supercell, попробуйте позже"));
        }

        return List.of(new MessageWrapper(text.toString()));
    }
}
