package vicePresident.Service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vicePresident.Communication.ClubCommunication;
import vicePresident.Entity.Club;
import vicePresident.Entity.ClubSettings;
import vicePresident.Entity.Player;
import vicePresident.Entity.Status;
import vicePresident.Repository.ClubRepository;
import vicePresident.TelegramBot.ConstantMessages;
import vicePresident.TelegramBot.TelegramBot;
import vicePresident.Wrapper.BrawlStarsEntity.BrawlStarsClub;
import vicePresident.Wrapper.BrawlStarsEntity.BrawlStartClubMember;
import vicePresident.Wrapper.MessageWrapper;

import java.net.URISyntaxException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

@Service
public class ClubService {
    @Autowired
    private ClubCommunication clubCommunication;

    @Autowired
    private ClubRepository clubRepo;

    private static Logger LOG = LoggerFactory.getLogger(ClubService.class);

    @Transactional
    public List<MessageWrapper> registerClub(String tag, Long chatID) {
        // checking is club registered already
        if (clubRepo.getClubByTag(tag) != null || clubRepo.getClubByTGID(chatID) != null) {
            return List.of(new MessageWrapper("Этот клуб уже зарегистрирован или к вашей группе привязан другой клуб.\nВоспользуйтесь командой /unlink и повторите попытку"));
        }

        AtomicReference<BrawlStarsClub> club = new AtomicReference<>();
        boolean[] successfullyFetched = {true};

        Thread thread = new Thread(() -> {
            try {
                club.set(clubCommunication.getClubByTag(tag));
            } catch (URISyntaxException e) {
                LOG.error(e.getMessage());
            }
        });

        thread.setName(tag + " club fetching");
        thread.setPriority(Thread.MAX_PRIORITY);

        thread.setUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread t, Throwable e) {
                LOG.error(e.getMessage());
                successfullyFetched[0] = false;
            }
        });
        thread.start();
        while (thread.isAlive()){} // waiting thread

        if (!successfullyFetched[0]) {
            return List.of(
                    new MessageWrapper(ConstantMessages.FETCH_CLUB_ERROR.formatted(tag.toUpperCase())));
        } else {
            Club newClub = new Club(chatID, tag);
            clubRepo.saveClub(newClub);

            List<MessageWrapper> messages = new ArrayList<>();
            messages.add(new MessageWrapper(ConstantMessages.CLUB_REGISTERED.formatted(club.get().getName())));

            // detect unregistered club members
            String members = ConstantMessages.PLAYERS_LIST.formatted(club.get().getMembers().size());

            List<String> texts = club
                    .get()
                    .getMembers()
                    .stream()
                    .map(BrawlStartClubMember::getName).toList();

            List<String> dates = club
                    .get()
                    .getMembers()
                    .stream()
                    .map(m -> "reg_" + m.getTag()).toList();

            Map<String,String> keyboard = new HashMap<>();
            for (int i = 0; i < texts.size(); i++) {
                keyboard.put(texts.get(i), dates.get(i));
            }

            messages.add(new MessageWrapper(members, TelegramBot.markupMaker(keyboard)));
            return messages;
        }
    }

    @Transactional
    public List<MessageWrapper> unlinkClub(Long chatID) {
        clubRepo.unlinkClub(chatID);
        return List.of(new MessageWrapper("Клуб, привязанный к вашему чату удален."));
    }

    @Transactional
    public List<MessageWrapper> checkUnregisteredPlayers(Long clubChatID) {
        Club club = clubRepo.getClubByTGID(clubChatID);
        BrawlStarsClub bsClub = new BrawlStarsClub();
        try {
             bsClub = clubCommunication.getClubByTag(club.getInGameTag());
        } catch (URISyntaxException e) {LOG.error(e.getMessage());}
        List<String> tags = club.getPlayers()
                .stream()
                .map(Player::getInGameTag)
                .toList();

        List<String> unregisteredNames = bsClub
                .getMembers()
                .stream()
                .filter(m -> !tags.contains(m.getTag()))
                .map(BrawlStartClubMember::getName)
                .toList();

        if (unregisteredNames.isEmpty()) {
            return List.of(new MessageWrapper("Все игроки в клубе зарегистрированы!"));
        }

        List<String> unregisteredTags = bsClub
                .getMembers()
                .stream()
                .filter(m -> !tags.contains(m.getTag()))
                .map(BrawlStartClubMember::getTag)
                .toList();

        Map<String,String> markupKeyboard = new HashMap<>();
        for (int i = 0; i < unregisteredNames.size(); i++) {
            markupKeyboard.put(unregisteredNames.get(i), "reg_" + unregisteredTags.get(i));
        }
        return List.of(new MessageWrapper("Вот список игроков, которые еще не прошли регистрацию в ваш клуб. \nЕсли игроков все еще слишком много, список может быть не полон",
                TelegramBot.markupMaker(markupKeyboard)));
    }

    @Transactional
    public MessageWrapper switchNotify(Long chatID) {
        Club club = clubRepo.getClubByTGID(chatID);
        clubRepo.switchNotify(club.getSettings());

        return new MessageWrapper(
          club.getSettings().isNotify() ? "Уведомления включены" : "Уведомления выключены");
    }

    @Transactional
    public MessageWrapper changeNotifyText(Long chatID, String text, String field) {
        Club club = clubRepo.getClubByTGID(chatID);
        ClubSettings settings = club.getSettings();
        boolean league = field.equals("league");

        if (league) {
            settings.setLeagueText(text);
        } else {
            settings.setQuestsText(text);
        }

        clubRepo.saveClub(club);

        return new MessageWrapper("Текст уведомления при начале " + (league ? "клубной лиги" : "квестов клуба") +" изменен!");
    }

    @Transactional
    public Club getClubByChatID(Long chatID) {
        return clubRepo.getClubByTGID(chatID);
    }

    @Transactional
    public List<MessageWrapper> startLeagueCycle(Long chatID) {
        LOG.debug("starting league cycle for club with chatID " + chatID);
        Club club = clubRepo.getClubByTGID(chatID);
        ClubSettings settings = club.getSettings();
        Timestamp currDate = new Timestamp(new Date().getTime());
        settings.setLastStatusUpdate(currDate);
        settings.setStatus(new Status(3,"first_day"));

        club.setSettings(settings);
        clubRepo.saveClub(club);

        return List.of(new MessageWrapper("Вы начали цикл в своем клубе! Начало клубной лиги в вашем клубе начинается в " + currDate.getHours() + ":00"));
    }

    @Transactional
    public void saveClub(Club club) {
        clubRepo.saveClub(club);
    }
    public List<Club> getAllClubsTransactional() {
        return clubRepo.getAllClubsTransactional();
    }

    @Transactional
    public String getClubStatsByChatID(Long chatID) {
        Club clubByChatID = getClubByChatID(chatID);
        LOG.debug("getting stats of club with tag " + clubByChatID.getInGameTag());
        try {
            BrawlStarsClub club = clubCommunication.getClubByTag(clubByChatID.getInGameTag());

            // todo добавить тип клуба
            return """
                    <b>%s</b> (%s)
                    
                    %s
                    
                    🏆%d (Для входа требуется %d🏆)
                    """.formatted(
                    club.getName(),
                    club.getTag(),
                    club.getDescription(),
                    club.getTrophies(),
                    club.getRequiredTrophies());
        } catch (URISyntaxException e) {
            LOG.error(e.getMessage());
            return "Ошибка со стороны сервера :(";
        }
    }
}