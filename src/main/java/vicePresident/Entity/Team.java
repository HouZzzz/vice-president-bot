package vicePresident.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "team")
public class Team {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private int id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "club_id")
    private Club club;

    @Column(name = "played_today")
    private boolean playedToday;

    @Column(name = "earned_trophies")
    private int earnedTrophies;

    @OneToMany(mappedBy = "team",cascade = CascadeType.ALL,fetch = FetchType.EAGER)
    private List<Player> players = new ArrayList<>();

    public Team() {}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Club getClub() {
        return club;
    }

    public void setClub(Club club) {
        this.club = club;
    }

    public boolean isPlayedToday() {
        return playedToday;
    }

    public void setPlayedToday(boolean playedToday) {
        this.playedToday = playedToday;
    }

    public int getEarnedTrophies() {
        return earnedTrophies;
    }

    public void setEarnedTrophies(int earnedTrophies) {
        this.earnedTrophies = earnedTrophies;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }

    @Override
    public String toString() {
        return "Team{" +
                "id=" + id +
                ", club=" + club +
                ", playedToday=" + playedToday +
                ", earnedTrophies=" + earnedTrophies +
                '}';
    }
}
