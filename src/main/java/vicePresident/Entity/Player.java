package vicePresident.Entity;

import javax.persistence.*;

@Entity
@Table(name = "player")
public class Player {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private int id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "club_id")
    private Club club;

    @ManyToOne(fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    @JoinColumn(name = "team_id")
    private Team team;

    @Column(name = "telegram_id")
    private long telegramID;

    @Column(name = "ingame_tag")
    private String inGameTag;

    public Player(Club club, Team team, long telegramID, String inGameTag) {
        this.club = club;
        this.team = team;
        this.telegramID = telegramID;
        this.inGameTag = inGameTag;
    }

    public Player() {}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Club getClub() {
        return club;
    }

    public void setClub(Club club) {
        this.club = club;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    public long getTelegramID() {
        return telegramID;
    }

    public void setTelegramID(long telegramID) {
        this.telegramID = telegramID;
    }

    public String getInGameTag() {
        return inGameTag;
    }

    public void setInGameTag(String inGameTag) {
        this.inGameTag = inGameTag;
    }

    @Override
    public String toString() {
        return "Player{" +
                "id=" + id +
                ", telegramID=" + telegramID +
                ", inGameTag='" + inGameTag + '\'' +
                '}';
    }
}
