package vicePresident.Entity;

import javax.annotation.processing.Generated;
import javax.persistence.*;
import java.util.Map;

@Entity
@Table(name = "club_status")
public class Status {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private int id;

    @Column
    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Status() {
    }

    public Status(int id,String status) {
        this.id = id;
        this.status = status;
    }

    @Override
    public String toString() {
        return status;
    }

    public static Map<String, Integer> statusesID = Map.of(
            "quests",1,
            "quests_end", 2,
            "first_day",3,
            "first_day_end",4,
            "second_day",5,
            "second_day_end",6,
            "third_day",7,
            "third_day_end",8
    );

    public static Map<Integer, Status> getNextStatus = Map.of(
            8, new Status(1,"quests"),
            1, new Status(2,"quests_end"),
            2, new Status(3,"first_day"),
            3, new Status(4,"first_day_end"),
            4, new Status(5,"second_day"),
            5, new Status(6,"second_day_end"),
            6, new Status(7,"third_day_end"),
            7, new Status(8,"third_day_end")
    );

    public static Map<Integer, Integer> hoursUntilNextEvent = Map.of(
            1, 198,
            2, 30,
            3, 24,
            4, 24,
            5,24,
            6,24,
            7,24,
            8, 18

    );
}
