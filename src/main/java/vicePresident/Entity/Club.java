package vicePresident.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "club")
public class Club {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private int id;

    @Column(name = "telegram_chat_id")
    private long chatID;

    @Column(name = "ingame_tag")
    private String inGameTag;

    @OneToMany(cascade = CascadeType.ALL,mappedBy = "club",fetch = FetchType.EAGER)
    private List<Player> players;

    @OneToMany(cascade = CascadeType.ALL,mappedBy = "club",fetch = FetchType.EAGER)
    private List<Team> teams;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "setting_id")
    private ClubSettings settings = new ClubSettings();

    public Club(long chatID, String inGameTag) {
        this.chatID = chatID;
        this.inGameTag = inGameTag;
    }

    public Club() {}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getChatID() {
        return chatID;
    }

    public void setChatID(long chatID) {
        this.chatID = chatID;
    }

    public String getInGameTag() {
        return inGameTag;
    }

    public void setInGameTag(String inGameTag) {
        this.inGameTag = inGameTag;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }

    public ClubSettings getSettings() {
        return settings;
    }

    public void setSettings(ClubSettings settings) {
        this.settings = settings;
    }

    public List<Team> getTeams() {
        return teams;
    }

    public void setTeams(List<Team> teams) {
        this.teams = teams;
    }

    @Override
    public String toString() {
        return "Club{" +
                "id=" + id +
                ", chatID=" + chatID +
                ", inGameTag='" + inGameTag + '\'' +
                '}';
    }
}
