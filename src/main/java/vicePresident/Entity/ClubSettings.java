package vicePresident.Entity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.OffsetDateTime;

@Entity
@Table(name = "club_setting")
public class ClubSettings {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private int id;

    @Column(name = "league_text")
    private String leagueText = "Клубная лига началась! Успейте потратить билеты и принести клубу как можно больше очков за 24 часа!";

    @Column(name = "quests_text")
    private String questsText = "Начались квесты клубов! Успейте выполнить как можно больше квестов клуба за ближайшую неделю!";

    @Column(name = "status_update_time")
    private Timestamp lastStatusUpdate;

    @OneToOne(mappedBy = "settings",fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    private Club club;

    @Column(name = "notify")
    private boolean notify = true;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "status_id")
    private Status status;

    public ClubSettings() {}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLeagueText() {
        return leagueText;
    }

    public void setLeagueText(String leagueText) {
        this.leagueText = leagueText;
    }

    public String getQuestsText() {
        return questsText;
    }

    public void setQuestsText(String questsText) {
        this.questsText = questsText;
    }


    public Timestamp getLastStatusUpdate() {
        return lastStatusUpdate;
    }

    public void setLastStatusUpdate(Timestamp lastStatusUpdate) {
        this.lastStatusUpdate = lastStatusUpdate;
    }

    public Club getClub() {
        return club;
    }

    public void setClub(Club club) {
        this.club = club;
    }

    public boolean isNotify() {
        return notify;
    }

    public void setNotify(boolean notify) {
        this.notify = notify;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "ClubSettings{" +
                "id=" + id +
                ", leagueText='" + leagueText + '\'' +
                ", questsText='" + questsText + '\'' +
                ", status=" + status +
                ", lastStatusUpdate=" + lastStatusUpdate +
                ", notify=" + notify +
                '}';
    }
}
