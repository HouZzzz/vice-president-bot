package vicePresident.TelegramBot;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.telegram.telegrambots.bots.DefaultBotOptions;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.ParseMode;
import org.telegram.telegrambots.meta.api.methods.groupadministration.GetChatAdministrators;
import org.telegram.telegrambots.meta.api.methods.polls.SendPoll;
import org.telegram.telegrambots.meta.api.methods.polls.StopPoll;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.chatmember.ChatMember;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import vicePresident.Entity.Club;
import vicePresident.Service.ClubService;
import vicePresident.Wrapper.MessageWrapper;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class TelegramBot extends TelegramLongPollingBot {
    @Autowired
    MainHandler mainHandler;

    @Autowired
    private ClubService clubService;

    private static Logger LOG = LoggerFactory.getLogger(TelegramBot.class);

    @Override
    public void onUpdateReceived(Update update) {
        List<MessageWrapper> messages = new ArrayList<>();
        Long chatID = 0L;
        Message message = update.getMessage();
        // creating message
        if (update.hasPollAnswer()) {
            messages = mainHandler.handlePollAnswer(update.getPollAnswer());
            chatID = message.getChatId();

//            try {
//                LOG.debug("stopping poll");
//                execute(new StopPoll(chatID + "",update.getMessage().getMessageId()));
//            } catch (TelegramApiException e) {
//                LOG.error(e.getMessage());
//            }
        } else if (update.hasCallbackQuery()) {
            messages = mainHandler.handleCallbackQuery(update.getCallbackQuery());
            chatID = update.getCallbackQuery().getMessage().getChatId();
        }
        else if (message.isCommand()) {
            // notify
            if (message.getText().startsWith("/notifyallclubs")) {
                if (message.getFrom().getId() != 1016872254) return; // only bot owner can notify clubs
                List<Club> clubs = clubService.getAllClubsTransactional();
                String notify = message.getText().replaceFirst("/notifyallclubs","");

                clubs.forEach(club -> sendMessage(club.getChatID(),new MessageWrapper(notify)));

                LOG.info(clubs.size() + " clubs was notified");
                sendMessage(1016872254L,new MessageWrapper("Клубы уведомлены"));
                return;
            }

            // else
            try {
                List<ChatMember> admins = execute(GetChatAdministrators.builder()
                        .chatId(update.getMessage().getChatId())
                        .build());

                messages = mainHandler.handleCommand(message,admins);
                chatID = update.getMessage().getChatId();
            } catch (TelegramApiException e) {
                LOG.error(e.getMessage());
            }
        }
        else if (message.getLeftChatMember() != null) {
            messages = mainHandler.handleLeftUser(message.getLeftChatMember());
            chatID = update.getMessage().getChatId();
        }
        else if (message.getNewChatMembers() != null && !message.getNewChatMembers().isEmpty()) {
            messages = mainHandler.handleNewChatMember(message.getNewChatMembers(),message.getChatId());
            chatID = update.getMessage().getChatId();
        }

        if (messages.get(0).isPoll()) {
            MessageWrapper poll = messages.get(0);
            sendPoll(
                    poll.getChatID(),
                    poll.getQuestion(),
                    poll.getOptions(),
                    poll.isMultianswer());
            return;
        }

        // sending
        LOG.trace("sending messages: " + messages + " to " + chatID);
        for (MessageWrapper msg : messages) {
            try {
                execute(SendMessage.builder()
                        .chatId(chatID)
                        .text(msg.getMessage())
                        .replyMarkup(msg.getMarkups())
                        .parseMode(ParseMode.HTML)
                        .build());
            } catch (TelegramApiException e) {
                LOG.error(e.getMessage());
            }
        }
    }
    @Override
    public String getBotUsername() {
        return "@vicePresidentBsBot";
    }

    public TelegramBot(DefaultBotOptions options) {
        super(options);
    }

    @Override
    public String getBotToken() {
        return "6145209311:AAFfSsRp15MP8C4a7FxYcOPvPQdUxUD-Ffc";
    }

                                //                T: text V: data
    public static InlineKeyboardMarkup markupMaker(Map<String,String> textAndData) {
        InlineKeyboardMarkup markup = new InlineKeyboardMarkup();

        List<List<InlineKeyboardButton>> buttons = new ArrayList<>();

        List<InlineKeyboardButton> row = new ArrayList<>();
        int btnsOnRow = 0;
        for (String text : textAndData.keySet()) {
            if (btnsOnRow < 3) {
                InlineKeyboardButton button = new InlineKeyboardButton();
                button.setCallbackData(textAndData.get(text));
                button.setText(text);

                row.add(button);
                btnsOnRow++;
            } else {
                buttons.add(row);
                row = new ArrayList<>();
                btnsOnRow = 0;
            }
        }
        if (!row.isEmpty()) buttons.add(row);

        markup.setKeyboard(buttons);
        return markup;
    }

    public void sendMessage(Long chatID,MessageWrapper message) {
        try {
            execute(SendMessage
                    .builder()
                    .chatId(chatID)
                    .text(message.getMessage())
                    .replyMarkup(message.getMarkups())
                    .parseMode(ParseMode.HTML)
                    .build());
        } catch (TelegramApiException e) {
            LOG.error(e.getMessage());
        }
    }
    public void sendPoll(Long chatID, String question, List<String> options,boolean multianswer) {
        try {
            execute(SendPoll
                    .builder()
                    .chatId(chatID)
                    .allowMultipleAnswers(multianswer)
                    .isAnonymous(false)
                    .question(question)
                    .options(options)
                    .build());
        } catch (TelegramApiException e) {
            LOG.error(e.getMessage());
        }
    }
}
