package vicePresident.TelegramBot;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.objects.User;
import vicePresident.Service.ClubService;
import vicePresident.Service.PlayerService;
import vicePresident.Wrapper.MessageWrapper;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class UsersHandler {
    @Autowired
    private ClubService clubService;

    @Autowired
    private PlayerService playerService;

    private static Logger LOG = LoggerFactory.getLogger(UsersHandler.class);

    protected List<MessageWrapper> handleNewChatMember(List<User> members, Long chatID) {
        LOG.debug("handling new chat members: " + members.stream().map(User::getUserName).collect(Collectors.joining(", ")));
        List<MessageWrapper> messages = new ArrayList<>();

        boolean isVicePresidentBotInvited = members
                .stream()
                .anyMatch(member -> member.getId() == 6145209311L);
        if (isVicePresidentBotInvited) {
            LOG.info("vice president bot invited in group");
            messages.add(new MessageWrapper(ConstantMessages.START_MESSAGE));
        } else {
            members.forEach(user -> {
                MessageWrapper message = clubService.checkUnregisteredPlayers(chatID).get(0);
                message.setMessage("Привет, @" + user.getUserName() + "\n" +
                        "Найди в списке свой никнейм и нажми на него для регистрации.\n" +
                        "Если список слишком большой и твоего ника там не нашлось попробуй позже написав команду /check");
                messages.add(message);
            });
        }
        return messages;
    }

    public List<MessageWrapper> handleLeftUser(User user) {
        playerService.deletePlayer(user.getId());
        return List.of(new MessageWrapper("Пользователь " + user.getUserName() + " удален из клуба"));
    }
}
