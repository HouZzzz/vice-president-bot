package vicePresident.TelegramBot;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.objects.CallbackQuery;
import vicePresident.Service.PlayerService;
import vicePresident.Wrapper.MessageWrapper;

import java.util.List;

@Component
public class CallbackHandler {
    @Autowired
    private PlayerService playerService;

    private static Logger LOG = LoggerFactory.getLogger(CallbackHandler.class);

    protected List<MessageWrapper> handleCallbackQuery(CallbackQuery callbackQuery) {
        LOG.debug("handling callback with data " + callbackQuery.getData());
        if (callbackQuery.getData().startsWith("reg_")) {
            return playerService.registerPlayer(
                    callbackQuery.getData().split("_")[1],
                    callbackQuery.getFrom().getId(),
                    callbackQuery.getMessage().getChatId(),
                    callbackQuery.getFrom().getUserName());

        } else if (callbackQuery.getData().equals("myteam")) {
            return playerService.getPlayerTeam(callbackQuery.getFrom());
        } else if (callbackQuery.getData().startsWith("stat_")) {
            return List.of(new MessageWrapper(playerService.getStringifyPlayerByTag(callbackQuery.getData().replaceAll("stat_",""))));
        }

        return List.of(new MessageWrapper("Ошибка со стороны сервера :("));
    }
}
