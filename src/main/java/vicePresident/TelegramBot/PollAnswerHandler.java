package vicePresident.TelegramBot;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.objects.polls.PollAnswer;
import vicePresident.Service.TeamService;
import vicePresident.Wrapper.MessageWrapper;

import java.util.List;

@Component
public class PollAnswerHandler {
    @Autowired
    private TeamService teamService;

    private static Logger LOG = LoggerFactory.getLogger(PollAnswerHandler.class);

    protected List<MessageWrapper> handlePollAnswer(PollAnswer poll) {
        LOG.debug("handling poll answer");
        return List.of(new MessageWrapper("smth"));
    }
}
