package vicePresident.TelegramBot;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.objects.CallbackQuery;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.api.objects.chatmember.ChatMember;
import org.telegram.telegrambots.meta.api.objects.polls.PollAnswer;
import vicePresident.Wrapper.MessageWrapper;

import java.util.List;

@Component
public class MainHandler {
    @Autowired
    private CommandHandler commandHandler;

    @Autowired
    private CallbackHandler callbackHandler;

    @Autowired
    private UsersHandler usersHandler;

    @Autowired
    private PollAnswerHandler pollAnswerHandler;
    private static Logger LOG = LoggerFactory.getLogger(MainHandler.class);

    protected List<MessageWrapper> handleCommand(Message message, List<ChatMember> admins) {
        LOG.debug("handling command: " + message.getText());
        String[] splittedCommand = message.getText()
                .replaceAll("@vicePresidentBsBot","")
                .split(" ");
        switch (splittedCommand[0]) {
            case "/link" -> {
                return commandHandler.linkCommand(splittedCommand,message.getChatId(),message,admins);
            }
            case "/unlink" -> {
                return commandHandler.unlinkCommand(message, admins);
            }
            case "/check" -> {
                return commandHandler.checkCommand(message.getChatId());
            }
            case "/help" -> {
                return List.of(new MessageWrapper(ConstantMessages.HELP));
            }
            case "/notify" -> {
                return commandHandler.notifyCommand(message, admins);
            }
            case "/leaguetext" -> {
                return commandHandler.changeNotifyText("league",message, admins);
            }
            case "/queststext" -> {
                return commandHandler.changeNotifyText("quests",message, admins);
            }
            case "/teams" -> {
                return commandHandler.teamsCommand(message.getChatId());
            }
            case "/week" -> {
                return commandHandler.weekCommand(message.getChatId());
            }
            case "/myteam" -> {
                return commandHandler.myTeamCommand(message.getFrom());
            }
            case "/changeteam" -> {
                return commandHandler.changeteamCommand(message.getFrom().getId());
            }
            case "/play" -> {
                List<Object> args = commandHandler.playCommand(message.getFrom().getId());
                LOG.debug("poll args: " + args);

                if (args.get(0) instanceof Boolean && !(boolean) args.get(0)) {
                    return List.of(new MessageWrapper("Ваша команда уже играла сегодня."));
                }
                return List.of(new MessageWrapper(
                        message.getChatId(),
                        (String) args.get(1),
                        (List<String>) args.get(2),
                        (Boolean) args.get(3)));
            }
            case "/club" -> {
                return List.of(new MessageWrapper(commandHandler.clubCommand(message.getChatId())));
            }
        }

        return List.of(new MessageWrapper("Неизвестная команда. Узнайте о возможностях бота и его командах с помощью /help"));
    }
    protected List<MessageWrapper> handleCallbackQuery(CallbackQuery callbackQuery) {
        return callbackHandler.handleCallbackQuery(callbackQuery);
    }

    protected List<MessageWrapper> handleLeftUser(User leftChatMember) {
        return usersHandler.handleLeftUser(leftChatMember);
    }
    protected List<MessageWrapper> handleNewChatMember(List<User> newChatMembers,Long chatID) {
        return usersHandler.handleNewChatMember(newChatMembers,chatID);
    }
    protected List<MessageWrapper> handlePollAnswer(PollAnswer pollAnswer) {
        return pollAnswerHandler.handlePollAnswer(pollAnswer);
    }
}
