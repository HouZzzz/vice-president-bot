package vicePresident.TelegramBot;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.api.objects.chatmember.ChatMember;
import vicePresident.Annotations.AdminRequired.AdminRequired;
import vicePresident.Entity.Team;
import vicePresident.Service.ClubService;
import vicePresident.Service.PlayerService;
import vicePresident.Service.TeamService;
import vicePresident.Wrapper.MessageWrapper;

import java.util.List;

@Component
public class CommandHandler {
    @Autowired
    private ClubService clubService;

    @Autowired
    private PlayerService playerService;

    @Autowired
    private TeamService teamService;

    private static Logger LOG = LoggerFactory.getLogger(CommandHandler.class);


    protected List<Object> playCommand(Long telegramID) {
        List<Object> args = playerService.play(telegramID);
        if (args == null) {
            return List.of(false);
        }
        return args;
    }

    @AdminRequired
    protected List<MessageWrapper> linkCommand(String[] commandArgs, Long chatID,Message message, List<ChatMember> admins) {
        if (commandArgs.length < 2) {
            LOG.error("cant link club: enough arguments");
            return List.of(new MessageWrapper("Введите тег после команды.\nПример: <code>/link #ABC123</code>"));
        }
        return clubService.registerClub(commandArgs[1],chatID);
    }

    protected List<MessageWrapper> checkCommand(Long chatID) {
        return clubService.checkUnregisteredPlayers(chatID);
    }

    @AdminRequired
    protected List<MessageWrapper> unlinkCommand(Message message, List<ChatMember> admins) {
        return clubService.unlinkClub(message.getChatId());
    }

    protected List<MessageWrapper> teamsCommand(Long chatId) {
        List<Team> teams = teamService.getTeamsByClubChatID(chatId);
        if (teams.size() == 0) return List.of(new MessageWrapper("В вашем клубе пока что нету команд"));

        return teamService.teamListToMessage(teams);
    }

    protected List<MessageWrapper> myTeamCommand(User from) {
        return playerService.getPlayerTeam(from);
    }

    protected List<MessageWrapper> weekCommand(Long chatID) {
        return clubService.startLeagueCycle(chatID);
    }

    @AdminRequired
    protected List<MessageWrapper> notifyCommand(Message message, List<ChatMember> admins) {
        return List.of(clubService.switchNotify(message.getChatId()));
    }

    @AdminRequired
    protected List<MessageWrapper> changeNotifyText(String field,Message message, List<ChatMember> admins) {
        String[] args = message.getText().split(" ");
        if (args.length < 2) {
            return List.of(new MessageWrapper("Вы не указали текст. Напишите его после пробела"));
        }
        String text = message
                .getText()
                .replace("/leaguetext ", "")
                .replace("/queststext ", "");

        return List.of(clubService.changeNotifyText(message.getChatId(), text,field));
    }

    protected List<MessageWrapper> changeteamCommand(Long telegramID) {
        Team team = playerService.changePlayerTeam(telegramID);
        return List.of(new MessageWrapper("Найдена новая команда для <a href=\"tg://user?id=%d\">вас</a>!\nВоспользуйтесь командой /myteam для просмотра состава вашей команды".formatted(telegramID)));
    }

    protected String clubCommand(Long chatId) {
        return clubService.getClubStatsByChatID(chatId);
    }
}
