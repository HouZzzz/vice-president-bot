package vicePresident.AOP;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.chatmember.ChatMember;
import vicePresident.Annotations.AdminRequired.AdminRequired;
import vicePresident.TelegramBot.ConstantMessages;
import vicePresident.Wrapper.MessageWrapper;

import java.lang.reflect.Method;
import java.util.List;

@Component
@Aspect
public class AnnotationsProcessor {
    private static Logger LOG = LoggerFactory.getLogger(AnnotationsProcessor.class);

    @Around("execution(* vicePresident.TelegramBot..*(..))")
    private Object handlersAdvice(ProceedingJoinPoint joinPoint) throws Throwable {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();
        if (method.isAnnotationPresent(AdminRequired.class)) {
            LOG.debug("checking for sent from admin");
            Object[] args = joinPoint.getArgs();
            int length = args.length;

            if (isFromAdmin((Message) args[length - 2],(List<ChatMember>) args[length - 1])) {
                return joinPoint.proceed(args);
            } else {
                return ConstantMessages.HAVE_NO_RIGHTS;
            }
        }
        return joinPoint.proceed();
    }

    private boolean isFromAdmin(Message message, List<ChatMember> admins) {
        List<Long> adminsID = admins
                .stream()
                .map(m -> m.getUser().getId())
                .toList();
        return adminsID.contains(message.getFrom().getId());
    }
}
