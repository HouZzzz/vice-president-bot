package vicePresident.Configuration;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.client.RestTemplate;
import org.telegram.telegrambots.bots.DefaultBotOptions;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.updatesreceivers.DefaultBotSession;
import vicePresident.TelegramBot.TelegramBot;

import javax.sql.DataSource;
import java.beans.PropertyVetoException;
import java.util.Properties;

@Configuration
@ComponentScan("vicePresident")
@EnableTransactionManagement
@EnableAspectJAutoProxy
public class MainConfig {
    private Logger LOG = LoggerFactory.getLogger(MainConfig.class);
    @Bean
    public DataSource dataSource() {
        ComboPooledDataSource dataSource = new ComboPooledDataSource();
        try {
            dataSource.setDriverClass("org.postgresql.Driver");
            dataSource.setJdbcUrl("jdbc:postgresql://localhost:5432/vice_president?options=-c%20statement_timeout=720min");
            dataSource.setUser("postgres");
            dataSource.setPassword("root");

            LOG.debug("data source bean created");
            return dataSource;
        } catch (PropertyVetoException e) {
            LOG.error(e.getMessage());
            throw new RuntimeException(e);
        }
    }

    @Bean
    public LocalSessionFactoryBean sessionFactory() {
        LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
        sessionFactory.setDataSource(dataSource());
        sessionFactory.setPackagesToScan("vicePresident.Entity");

        Properties hibernateProperties = new Properties();
        hibernateProperties.setProperty("hibernate.dialect", "org.hibernate.dialect.PostgreSQLDialect");
        hibernateProperties.setProperty("hibernate.show_sql", "false");

        sessionFactory.setHibernateProperties(hibernateProperties);
        LOG.debug("session factory bean created");
        return sessionFactory;
    }

    @Bean
    public TelegramBot bot() throws TelegramApiException {
        TelegramBot bot = new TelegramBot(new DefaultBotOptions());
        TelegramBotsApi api = new TelegramBotsApi(DefaultBotSession.class);
        api.registerBot(bot);

        LOG.info("bot created");
        return bot;
    }

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }
}
