package vicePresident.Updaters;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vicePresident.Entity.Club;
import vicePresident.Entity.Status;
import vicePresident.Entity.Team;
import vicePresident.Repository.ClubRepository;
import vicePresident.Service.ClubService;
import vicePresident.TelegramBot.TelegramBot;
import vicePresident.Wrapper.MessageWrapper;

import java.sql.Timestamp;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ClubEventUpdater {
    private TelegramBot bot;
    private ClubService clubService;

    private static Logger LOG = LoggerFactory.getLogger(ClubEventUpdater.class);

    // K: club tag, V: hours left to next event
    private Map<String,Integer> hoursLeft = new HashMap<>();


    private void updateData() {
        LOG.debug("loading data in club event updater");
        hoursLeft = new HashMap<>(); // clear

        Thread dataLoader = new Thread(() -> {
            while (true) {
                List<Club> clubs = clubService.getAllClubsTransactional()
                        .stream()
                        .filter(c -> c.getSettings().getStatus() != null) // empty status (never used /week command)
                        .filter(c -> c.getSettings().isNotify()) // notifies turned off
                        .toList();
                LOG.debug("found " + clubs.size() + " clubs with started cycle and toggled notifications");

                // fill hoursLeft
                clubs.forEach(club -> {
                    // find difference between current date and last status update
                    Timestamp currentDate = new Timestamp(new Date().getTime());
                    Timestamp lastUpdate = club.getSettings().getLastStatusUpdate();

                    long differenceInHours = (currentDate.getTime() - lastUpdate.getTime()) / 1000 / 60 / 60;
                    LOG.debug("DIH of club " + club.getInGameTag() + " is " + differenceInHours);

                    int hoursUtilNextEvent = Status.hoursUntilNextEvent.get(club.getSettings().getStatus().getId());

                    // detected event end
                    if (differenceInHours >= hoursUtilNextEvent) {
                        // skip N events if app didn't run long time
                        while (differenceInHours >= hoursUtilNextEvent) {
                            club.getSettings().setStatus(Status.getNextStatus.get(club.getSettings().getStatus().getId()));
                            differenceInHours -= hoursUtilNextEvent;
                            hoursUtilNextEvent = Status.hoursUntilNextEvent.get(club.getSettings().getStatus().getId());
                        }

                        club.getSettings().setLastStatusUpdate(currentDate);
                        clubService.saveClub(club);
                        if (!club.getSettings().getStatus().toString().endsWith("_end")) {
                            LOG.debug("notifying club");
                            Map<String, String> markupKeyboard = Map.of("Моя команда", "myteam");
                            bot.sendMessage(club.getChatID(),
                                    new MessageWrapper(
                                            club.getSettings().getStatus().toString().contains("quest") ?
                                                    club.getSettings().getQuestsText() : club.getSettings().getLeagueText(),
                                            TelegramBot.markupMaker(markupKeyboard)));
                        } else {
                            LOG.debug("resetting teams data");
                            List<Team> clubTeams = club.getTeams();
                            clubTeams.forEach(team -> {
                                team.setEarnedTrophies(0);
                                team.setPlayedToday(false);

                                club.setTeams(clubTeams);
                                clubService.saveClub(club);
                            });
                        }
                    }
                });

                try {
                    int nextUpdate = (1000 * 60) * (61 - new Date().getMinutes());
                    LOG.debug("next update will be in " + (nextUpdate / 1000 / 60) + " min");
                    Thread.sleep(nextUpdate); // util next hour + 1 min

//                    Thread.sleep(1000 * 60); // 1 min
                } catch (InterruptedException e) {
                    LOG.error(e.getMessage());
                }
            }
        });
        dataLoader.setName("club notifier");
        dataLoader.setPriority(Thread.MIN_PRIORITY);
        dataLoader.start();
    }


    @Autowired
    public ClubEventUpdater(TelegramBot bot, ClubService clubService) {
        this.bot = bot;
        this.clubService = clubService;

        updateData();
    }
}
