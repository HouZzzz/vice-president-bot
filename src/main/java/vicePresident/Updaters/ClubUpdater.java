package vicePresident.Updaters;

import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vicePresident.Communication.ClubCommunication;
import vicePresident.Entity.Club;
import vicePresident.Entity.Player;
import vicePresident.Repository.ClubRepository;
import vicePresident.TelegramBot.TelegramBot;
import vicePresident.Wrapper.BrawlStarsEntity.BrawlStarsClub;
import vicePresident.Wrapper.BrawlStarsEntity.BrawlStartClubMember;
import vicePresident.Wrapper.MessageWrapper;

import java.net.URISyntaxException;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class ClubUpdater {
    private SessionFactory sessionFactory;
    private ClubCommunication clubCommunication;
    private ClubRepository clubRepo;

    private TelegramBot telegramBot;

    private static Logger LOG = LoggerFactory.getLogger(ClubUpdater.class);

    // players tags
    private List<String> dbClubs = new ArrayList<>();
    private List<String> bsClubs = new ArrayList<>();

    private void updateClubs() {
        Thread databaseFetcher = new Thread(() -> {
            while (true) {
                try {
                    // fetching new data
                    List<Club> fromDB = clubRepo.getAllClubsTransactional();
                    List<BrawlStarsClub> fromAPI = new ArrayList<>();
                    fromDB.forEach(c -> {
                        try {
                            fromAPI.add(clubCommunication.getClubByTag(c.getInGameTag()));
                        } catch (URISyntaxException e) {
                            LOG.error(e.getMessage());
                        }
                    });

                    // founding new players in each club
                    for (int i = 0; i < fromAPI.size(); i++) {
                        BrawlStarsClub apiClub = fromAPI.get(i);

                        List<String> newPlayers = apiClub
                                .getMembers()
                                .stream()
                                .filter(member -> !bsClubs.contains(member.getTag()))
                                .map(member -> member.getName())
                                .toList();

                        //          list has only one bscm
                        Map<String, List<BrawlStartClubMember>> memberByNick = apiClub.getMembers()
                                .stream()
                                .collect(Collectors.groupingBy(BrawlStartClubMember::getName));
                        LOG.info("club " + apiClub.getTag() + " analyzed");
                        LOG.debug("new players: " + newPlayers);

                        if (!newPlayers.isEmpty()) {
                            Map<String,String> keyboard = new HashMap<>();

                            newPlayers.forEach(p ->
                                    keyboard.put("Статистика игрока " + p,"stat_" + memberByNick.get(p).get(0).getTag()));


                            telegramBot.sendMessage(fromDB.get(i).getChatID(),new MessageWrapper("В ваш клуб зашли новые игроки!\n\n" + String.join("\n", newPlayers),
                                    TelegramBot.markupMaker(keyboard)));
                        }
                    }

                    bsClubs = fromAPI
                            .stream()
                            .map(club -> club.getMembers())
                            .flatMap(Collection::stream)
                            .map(member -> member.getTag())
                            .toList();

                    loadDBClubs();
                    LOG.info("data updated");

                    Thread.sleep(1000 * 60 * 15); // once per 15 min
                } catch (InterruptedException e) {
                    LOG.error(e.getMessage());
                }
            }
        });

        databaseFetcher.setName("club updater");
        databaseFetcher.setPriority(Thread.MIN_PRIORITY);
        databaseFetcher.start();
    }

    private void loadData() {
        List<Club> fromDB = clubRepo.getAllClubsTransactional();
        LOG.debug("found list of clubs: " + fromDB);

        dbClubs = fromDB
                .stream()
                .map(Club::getPlayers)
                .flatMap(Collection::stream)
                .map(Player::getInGameTag)
                .toList();

        bsClubs = fromDB
                .stream()
                .map(club -> {
                    try {
                        return clubCommunication.getClubByTag(club.getInGameTag()).getMembers();
                    } catch (URISyntaxException e) {
                        LOG.error(e.getMessage());
                        return new BrawlStarsClub().getMembers();
                    }
                })
                .flatMap(Collection::stream)
                .map(member -> member.getTag())
                .toList();
        LOG.info("clubs data loaded");
    }

    private void loadDBClubs() {
        dbClubs = clubRepo.getAllClubsTransactional()
                .stream()
                .map(Club::getPlayers)
                .flatMap(Collection::stream)
                .map(Player::getInGameTag)
                .toList();
    }

    @Autowired
    public ClubUpdater(SessionFactory sessionFactory, ClubCommunication clubCommunication, ClubRepository clubRepo, TelegramBot telegramBot) {
        this.sessionFactory = sessionFactory;
        this.clubCommunication = clubCommunication;
        this.clubRepo = clubRepo;
        this.telegramBot = telegramBot;

        loadData();
        updateClubs();
    }
}
