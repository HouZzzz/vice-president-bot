package vicePresident.Wrapper.BrawlStarsEntity;

import java.util.List;

public class BrawlStarsPlayer {
    private String tag;
    private String name;
    private int trophies;
    private int highestTrophies;
    private int trioVictories;
    private int soloVictories;
    private int duoVictories;
    private BrawlStarsClub club;

    private List<Brawler> brawlers;

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTrophies() {
        return trophies;
    }

    public void setTrophies(int trophies) {
        this.trophies = trophies;
    }

    public int getHighestTrophies() {
        return highestTrophies;
    }

    public void setHighestTrophies(int highestTrophies) {
        this.highestTrophies = highestTrophies;
    }

    public int get3vs3Victories() {
        return trioVictories;
    }

    public void set3vs3Victories(int trioVictories) {
        this.trioVictories = trioVictories;
    }

    public int getSoloVictories() {
        return soloVictories;
    }

    public void setSoloVictories(int soloVictories) {
        this.soloVictories = soloVictories;
    }

    public int getDuoVictories() {
        return duoVictories;
    }

    public void setDuoVictories(int duoVictories) {
        this.duoVictories = duoVictories;
    }

    public BrawlStarsClub getClub() {
        return club;
    }

    public void setClub(BrawlStarsClub club) {
        this.club = club;
    }

    public List<Brawler> getBrawlers() {
        return brawlers;
    }

    public void setBrawlers(List<Brawler> brawlers) {
        this.brawlers = brawlers;
    }

    @Override
    public String toString() {
        return "BrawlStarsPlayer{" +
                "tag='" + tag + '\'' +
                ", name='" + name + '\'' +
                ", trophies=" + trophies +
                ", highestTrophies=" + highestTrophies +
                ", trioVictories=" + trioVictories +
                ", soloVictories=" + soloVictories +
                ", duoVictories=" + duoVictories +
                ", club=" + club +
                ", brawlers=" + brawlers +
                '}';
    }
}
