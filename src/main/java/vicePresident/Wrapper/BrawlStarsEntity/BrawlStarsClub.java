package vicePresident.Wrapper.BrawlStarsEntity;

import java.util.List;

public class BrawlStarsClub {
    private String tag;
    private String name;
    private String description;
    private int trophies;
    private int requiredTrophies;
    private String type;
    private List<BrawlStartClubMember> members;

    private String reason;

    public BrawlStarsClub(String tag, String name, List<BrawlStartClubMember> members) {
        this.tag = tag;
        this.name = name;
        this.members = members;
    }

    public BrawlStarsClub() {
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<BrawlStartClubMember> getMembers() {
        return members;
    }

    public void setMembers(List<BrawlStartClubMember> members) {
        this.members = members;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public int getTrophies() {
        return trophies;
    }

    public void setTrophies(int trophies) {
        this.trophies = trophies;
    }

    public int getRequiredTrophies() {
        return requiredTrophies;
    }

    public void setRequiredTrophies(int requiredTrophies) {
        this.requiredTrophies = requiredTrophies;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "BrawlStarsClub{" +
                "tag='" + tag + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", trophies=" + trophies +
                ", requiredTrophies=" + requiredTrophies +
                ", type='" + type + '\'' +
                ", members=" + members +
                ", reason='" + reason + '\'' +
                '}';
    }
}
