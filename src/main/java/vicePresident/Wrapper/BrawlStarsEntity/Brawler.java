package vicePresident.Wrapper.BrawlStarsEntity;

public class Brawler {
    private String name;
    private int power;
    private int rank;
    private int trophies;
    private int highestTrophies;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public int getTrophies() {
        return trophies;
    }

    public void setTrophies(int trophies) {
        this.trophies = trophies;
    }

    public int getHighestTrophies() {
        return highestTrophies;
    }

    public void setHighestTrophies(int highestTrophies) {
        this.highestTrophies = highestTrophies;
    }

    @Override
    public String toString() {
        return "Brawler{" +
                "name='" + name + '\'' +
                ", power=" + power +
                ", rank=" + rank +
                ", trophies=" + trophies +
                ", highestTrophies=" + highestTrophies +
                '}';
    }
}
