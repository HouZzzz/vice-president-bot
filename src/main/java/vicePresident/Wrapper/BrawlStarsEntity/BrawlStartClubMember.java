package vicePresident.Wrapper.BrawlStarsEntity;

public class BrawlStartClubMember {
    private String tag;
    private String name;
    private String role; // fixme use enum
    private int trophies;

    public BrawlStartClubMember(String tag, String name, String role) {
        this.tag = tag;
        this.name = name;
        this.role = role;
    }

    public BrawlStartClubMember() {
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public int getTrophies() {
        return trophies;
    }

    public void setTrophies(int trophies) {
        this.trophies = trophies;
    }

    @Override
    public String toString() {
        return "BrawlStartClubMember{" +
                "tag='" + tag + '\'' +
                ", name='" + name + '\'' +
                ", role='" + role + '\'' +
                ", trophies=" + trophies +
                '}';
    }
}
