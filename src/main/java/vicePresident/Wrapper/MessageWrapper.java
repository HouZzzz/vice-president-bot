package vicePresident.Wrapper;

import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;

import java.util.List;

public class MessageWrapper {
    private Long chatID;
    private String message;
    private boolean isPoll = false;
    private String question;
    private List<String> options;
    private boolean multianswer;

    private InlineKeyboardMarkup markups;

    public MessageWrapper(String message) {
        this.message = message;
    }

    public MessageWrapper(String message, InlineKeyboardMarkup markups) {
        this.message = message;
        this.markups = markups;
    }


    public MessageWrapper(Long chatID, String question, List<String> options, boolean multianswer) {
        isPoll = true;

        this.chatID = chatID;
        this.question = question;
        this.options = options;
        this.multianswer = multianswer;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public InlineKeyboardMarkup getMarkups() {
        return markups;
    }

    public void setMarkups(InlineKeyboardMarkup markups) {
        this.markups = markups;
    }

    @Override
    public String toString() {
        return "MessageWrapper{" +
                "message='" + message + '\'' +
                ", markups=" + markups +
                '}';
    }

    public Long getChatID() {
        return chatID;
    }

    public boolean isPoll() {
        return isPoll;
    }

    public String getQuestion() {
        return question;
    }

    public List<String> getOptions() {
        return options;
    }

    public boolean isMultianswer() {
        return multianswer;
    }
}
